<?php
/**
 * Created by PhpStorm.
 * User: arnajonasar
 * Date: 22/01/16
 * Time: 19:37
 */

$encodeEvents=array("location"=>array("San Francisco, CA","Austin, TX","New York, NY"),
    "date"=>array("May 1","May 15","May 30"),
    "map"=>array("img/map-ca.png","img/map-tx.png","img/map-ny.png"));

echo json_encode($encodeEvents);

/*
 * Útkoma:
{
    "location":
    [
        "San Francisco, CA",
        "Austin, TX",
        "New York, NY"
    ],
    "date":
    [
        "May 1",
        "May 15",
        "May 30"
    ],
    "map":
    [
        "img\/map-ca.png",
        "img\/map-tx.png",
        "img\/map-ny.png"
    ]
}
 */

$decodeEvents='{"events": [{"location":["San Francisco, CA","Austin, TX","New York, NY"],"date":["May 1","May 15", "May 30"],"map":["img/map-ca.png","img/map-tx.png","img/map-ny.png"]}]}';

var_dump(json_decode($decodeEvents, true));

/*
* Útkoma:


array(1)
{ ["events"]=> array(1)
    { [0]=> array(3)
        { ["location"]=> array(3)
            {
                [0]=> string(17) "San Francisco, CA"
                [1]=> string(10) "Austin, TX"
                [2]=> string(12) "New York, NY"
            }
            ["date"]=> array(3)
            {
                [0]=> string(5) "May 1"
                [1]=> string(6) "May 15"
                [2]=> string(6) "May 30"
            }
            ["map"]=> array(3)
            {
                [0]=> string(14) "img/map-ca.png"
                [1]=> string(14) "img/map-tx.png"
                [2]=> string(14) "img/map-ny.png"
            }
        }
    }
}

*/

